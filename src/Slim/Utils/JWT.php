<?php declare(strict_types = 1);

namespace DL2\Slim\Utils;

use DL2\Slim\Application;
use Firebase\JWT\JWT as FBJWT;

/**
 * Easily encode and decode JWT tokens.
 *
 * @todo(douggr): move to another lib?
 */
class JWT
{
    /** @var string[] */
    const ALGS = ['ES256', 'HS256', 'HS384', 'HS512', 'RS256', 'RS384', 'RS512'];

    /**
     * @param ?value-of<self::ALGS> $algo
     */
    public static function decode(string $token, ?string $secret = null, ?string $algo = null): object
    {
        if (!$algo || !$secret) {
            [$algo, $secret] = self::config();
        }

        return FBJWT::decode($token, $secret, [$algo]);
    }

    /**
     * @param array|object          $payload
     * @param ?value-of<self::ALGS> $algo
     */
    public static function encode($payload, ?string $secret = null, ?string $algo = null): string
    {
        if (!$algo || !$secret) {
            [$algo, $secret] = self::config();
        }

        $payload = \array_replace((array) $payload, ['iat' => \time()]);

        return FBJWT::encode($payload, $secret, $algo);
    }

    /**
     * @return array{string,string}
     */
    protected static function config(): array
    {
        /** @var array{algo:string,secret:string} */
        $jwt = Application::getInstance()
            ->getContainer()
            ->get('jwt')
        ;

        return [$jwt['algo'], $jwt['secret']];
    }
}
