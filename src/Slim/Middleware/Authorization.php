<?php declare(strict_types = 1);

namespace DL2\Slim\Middleware;

use DL2\Slim\Application;
use DL2\Slim\Exception;
use DL2\Slim\Utils\JWT;
use Slim\Http\Request;
use Slim\Http\Response;
use Throwable;
use UnexpectedValueException;

class Authorization
{
    /**
     * @psalm-param callable(Request,Response):Response $next
     *
     * @internal
     */
    public function __invoke(Request $req, Response $res, callable $next): Response
    {
        /** @var ?string */
        $authHeader = $req->getHeaderLine(\getenv('AUTHORIZATION_HEADER') ?: 'Authorization');

        if (!$authHeader) {
            return $next($req, $res);
        }

        [$bearer, $token] = \explode(' ', $authHeader);

        if (!$bearer || 'bearer' !== \strtolower($bearer)) {
            $error = [
                'message' => 'Please make sure your request has a valid “Authorization” header',
                'status'  => 400,
                'type'    => 'invalid',
            ];

            throw new Exception($error);
        }

        try {
            Application::getInstance()
                ->getContainer()
                ->offsetSet('identity', JWT::decode($token))
            ;
        } catch (UnexpectedValueException $err) {
            $error = [
                'data'    => ['detail' => $err->getMessage()],
                'message' => 'Invalid JWT',
                'status'  => 400,
                'type'    => 'invalid',
            ];

            throw new Exception($error);
        } catch (Throwable $err) {
            $error = [
                'message' => $err->getMessage(),
                'status'  => 401,
                'type'    => 'unauthorized',
            ];

            throw new Exception($error);
        }

        return $next($req, $res);
    }
}
