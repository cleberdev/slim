<?php declare(strict_types = 1);

namespace DL2\Slim;

use Slim\Http\Request;
use Slim\Http\Response;
use Throwable;

abstract class Controller
{
    protected const REQUIRES_AUTH = true;

    /** @var Application */
    protected $app;

    /**
     * ctor.
     */
    public function __construct()
    {
        $this->app = Application::getInstance();
        $this->init();
    }

    /**
     * Called _before_ the request is dispatched.
     *
     * @psalm-param callable(Request,Response):Response $next
     *
     * @internal
     */
    public static function preDispatch(Request $req, Response $res, callable $next): Response
    {
        if (!static::REQUIRES_AUTH) {
            return $next($req, $res);
        }

        try {
            // just test if the `identity` attribute is set
            Application::getInstance()->getIdentity();

            return $next($req, $res);
        } catch (Throwable $ex) {
            $error = [
                'message' => 'Could not verify that you are authorized to access the document requested.',
                'type'    => 'unauthorized',
            ];

            throw new Exception($error);
        }
    }

    /**
     * Called from `__construct()` as final step of object instantiation.
     */
    protected function init(): void
    {
        // intentionally left blank
    }

    /**
     * Called _after_ the request is dispatched.
     *
     * @param ?array<string,mixed> $args
     *
     * @internal
     */
    protected function postDispatch(Request $req, Response $res, ?array $args = []): Response
    {
        return $res;
    }

    /**
     * @param array<string,mixed> $args
     */
    public function __invoke(Request $req, Response $res, ?array $args): Response
    {
        $callback = \strtolower($req->getMethod());

        if (!\method_exists($this, $callback)) {
            throw new Exception(['message' => 'Not Found', 'type' => 'missing']);
        }

        /** @var Response */
        $res = \call_user_func_array([$this, $callback], [$req, $res, $args]);

        return $this->postDispatch($req, $res, $args);
    }
}
